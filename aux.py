import os
import re
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from collections import defaultdict

PUNCTUATION = ['.', ',', ':', ';', '?', '¿', '!', '¡']
PUNC_REGEX = '|'.join('\\' + p for p in PUNCTUATION)

dir_path = os.path.dirname(os.path.realpath(__file__))
STOP_WORDS_ES_PATH = os.path.join(dir_path, 'stop_words_es.txt')
STOP_WORDS_EN_PATH = os.path.join(dir_path, 'stop_words_en.txt')
with open(STOP_WORDS_ES_PATH, 'r') as f:
    STOP_WORDS_ES = [w.rstrip() for w in f]
with open(STOP_WORDS_EN_PATH, 'r') as f:
    STOP_WORDS_EN = [w.rstrip() for w in f]

def ver_codificacion(string):
    str_bytes = (c.encode('utf-8').hex() for c in string)
    print(' '.join(str_bytes))

def tokenizar(text):
    return text.split()

def sacar_puntuacion(text):
    return re.sub(PUNC_REGEX, ' ', text)

def _get_stop_words(lang):
    if lang == 'es':
        stop_words = STOP_WORDS_ES
    elif lang == 'en':
        stop_words = STOP_WORDS_EN
    else:
        raise "Idioma no reconocido."
    return stop_words

# Correr preferentemente con el output de sacar_puntuacion()
def sacar_stop_words(text, lang='es'):
    stop_words = _get_stop_words(lang)
    words = text.split()
    resultwords  = [word for word in words if word.lower() not in stop_words]
    filtered = ' '.join(resultwords)
    return filtered

def crear_frecuencias(tokenized, toLower=True):
    freq_dict = defaultdict(int)
    for token in tokenized:
        if toLower:
            token = token.lower()
        freq_dict[token] += 1
    return dict(freq_dict)
    
def ordenar(freq_dict, n=10):
    sorted_entries = sorted(freq_dict.items(), key=lambda item: item[1], reverse=True)
    i = n - 1
    for k, v in sorted_entries:
        print(f'{k}: {v}')
        if i <= 0:
            break
        i -= 1

def crear_nube(freq_dict):
    wc = WordCloud(width = 800, height = 800, 
                background_color ='white',
                min_font_size = 10)
    cloud = wc.fit_words(freq_dict)
    
    # plot the WordCloud image                        
    plt.figure(figsize = (8, 8), facecolor = None) 
    plt.imshow(cloud) 
    plt.axis("off") 
    plt.tight_layout(pad = 0) 
    plt.show() 
